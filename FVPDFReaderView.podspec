Pod::Spec.new do |s|
  s.name         = 'FVPDFReaderView'
  s.version      = '1.0.1'
  s.summary      = 'PDF reader based on UIPageViewController and MTPDF library.'
  s.homepage     = 'https://bitbucket.org/freevision/fvpdfreaderview'
  s.screenshots  = [
    'https://dl.dropboxusercontent.com/u/350675/FVPDFReaderView/iphone1.png',
    'https://dl.dropboxusercontent.com/u/350675/FVPDFReaderView/iphone2.png',
    'https://dl.dropboxusercontent.com/u/350675/FVPDFReaderView/ipad1.png',
    'https://dl.dropboxusercontent.com/u/350675/FVPDFReaderView/ipad2.png'
  ]

  s.license      = { :type => 'MIT', :file => 'LICENSE' }

  s.author       = { 'Ahmed Al Hafoudh' => 'alhafoudh@freevision.sk' }
  s.source       = { :git => '/Users/alhafoudh/Projects/FVPDFReaderView', :tag => '1.0.0' }

  s.platform     = :ios, '6.1'

  s.source_files = 'FVPDFReaderView/Classes/**/*.{h,m}'
  s.requires_arc = true

  s.dependency 'MTPDF', '~> 0.0.5'
  s.dependency 'TMCache', '~> 1.2.0'
end
