//
//  FVAppDelegate.h
//  FVPDFReaderView
//
//  Created by Ahmed Al Hafoudh on 23.7.2013.
//  Copyright (c) 2013 freevision s.r.o. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FVAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
