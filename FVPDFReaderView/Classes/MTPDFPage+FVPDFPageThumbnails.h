//
//  FVPDFPageThumbnails.h
//  FVPDFReaderView
//
//  Created by Ahmed Al Hafoudh on 24.7.2013.
//  Copyright (c) 2013 freevision s.r.o. All rights reserved.
//

#import "MTPDF.h"

@interface MTPDFPage (FVPDFPageThumbnails)

- (UIImage *)thumbnailImageWithSize:(CGSize)thumbSize;

@end
