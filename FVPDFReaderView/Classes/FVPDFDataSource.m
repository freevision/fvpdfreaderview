//
//  FVPDFDataSource.m
//  FVPDFReaderView
//
//  Created by Ahmed Al Hafoudh on 23.7.2013.
//  Copyright (c) 2013 freevision s.r.o. All rights reserved.
//

#import "FVPDFDataSource.h"

#import "FVPDFPageViewController.h"

@interface FVPDFDataSource()

@end

@implementation FVPDFDataSource

- (id)initWithPDF:(MTPDF *)pdf {
  self = [super init];
  if (self) {
    // Create the data model.
    
    _pdf = pdf;
    _viewControllers = [[NSMutableDictionary alloc] initWithCapacity:[_pdf.pages count]];
    _unusedViewControllers = [[NSMutableArray alloc] init];
    
  }
  return self;
}

- (FVPDFPageViewController *)loadDataViewController:(UIStoryboard *)storyboard index:(NSUInteger)index {
  if ([_viewControllers objectForKey:@(index)] != nil)
    return [_viewControllers objectForKey:@(index)];


  if ([_unusedViewControllers count] == 0)
    [_unusedViewControllers addObject:[storyboard instantiateViewControllerWithIdentifier:@"FVPDFPageViewController"]];
  
  FVPDFPageViewController *dataViewController = [_unusedViewControllers lastObject];
  
  [_unusedViewControllers removeLastObject];
  
  dataViewController.pdfPage = [_pdf.pages objectAtIndex:index];
  [dataViewController loadPage];
  
  [_viewControllers setObject:dataViewController forKey:@(index)];
  
  return dataViewController;
}

- (void)cleanViewControllerAtIndex:(NSUInteger)index {
  if ([_viewControllers objectForKey:@(index)] == nil)
    return;
  
  [_unusedViewControllers addObject:[_viewControllers objectForKey:@(index)]];
  [_viewControllers removeObjectForKey:@(index)];
}

- (FVPDFPageViewController *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard {
  // Return the data view controller for the given index.
  if (([_pdf.pages count] == 0) || (index >= [_pdf.pages count])) {
    return nil;
  }
 
  [self loadDataViewController:storyboard index:index];
  
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    int newIndex;
    
    // Load next
    newIndex = (int)index + 1;
    if (newIndex < [_pdf.pages count]) {
      [self loadDataViewController:storyboard index:newIndex];
    }

    // Load previous
    newIndex = (int)index - 1;
    if (newIndex >= 0) {
      [self loadDataViewController:storyboard index:newIndex];
    }
    
    int cleanIndex;
    
    // Clean 3rd
    cleanIndex = (int)index + 3;
    if (cleanIndex < [_pdf.pages count])
      [self cleanViewControllerAtIndex:cleanIndex];
    
    // Clean -3rd
    cleanIndex = (int)index - 3;
    if (cleanIndex >= 0)
      [self cleanViewControllerAtIndex:cleanIndex];
  });
  
  return [_viewControllers objectForKey:@(index)];
}

- (NSUInteger)indexOfViewController:(FVPDFPageViewController *)viewController {
     // Return the index of the given data view controller.
     // For simplicity, this implementation uses a static array of model objects and the view controller stores the model object; you can therefore use the model object to identify the index.
    return [_pdf.pages indexOfObject:viewController.pdfPage];
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
//  NSLog(@"%@", NSStringFromSelector(_cmd));
  
  NSUInteger index = [self indexOfViewController:(FVPDFPageViewController *)viewController];
  if ((index == 0) || (index == NSNotFound)) {
      return nil;
  }
  
  index--;
  return [self viewControllerAtIndex:index storyboard:viewController.storyboard];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
//  NSLog(@"%@", NSStringFromSelector(_cmd));
  
  NSUInteger index = [self indexOfViewController:(FVPDFPageViewController *)viewController];
  if (index == NSNotFound) {
      return nil;
  }
  
  index++;
  if (index == [_pdf.pages count]) {
      return nil;
  }
  return [self viewControllerAtIndex:index storyboard:viewController.storyboard];
}

@end
