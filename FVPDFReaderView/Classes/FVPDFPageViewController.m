//
//  FVPDFPageViewController.m
//  FVPDFReaderView
//
//  Created by Ahmed Al Hafoudh on 23.7.2013.
//  Copyright (c) 2013 freevision s.r.o. All rights reserved.
//

#import "FVPDFPageViewController.h"

@interface FVPDFPageViewController ()

@end

@implementation FVPDFPageViewController

- (void)loadPage {
  self.image = [self.pdfPage imageWithPixelsPerPoint:4];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  
  [self.contentView setImage:self.image];
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
}

@end
