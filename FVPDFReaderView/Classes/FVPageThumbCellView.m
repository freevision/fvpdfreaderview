//
//  FVPageThumbCellView.m
//  FVPDFReaderView
//
//  Created by Ahmed Al Hafoudh on 24.7.2013.
//  Copyright (c) 2013 freevision s.r.o. All rights reserved.
//

#import "FVPageThumbCellView.h"

@implementation FVPageThumbCellView

- (void)reset {
  self.backgroundColor = [UIColor clearColor];
  self.activityIndicatorView.alpha = 1.0;
  self.imageView.alpha = 0.0;
}

- (void)appearAnimated:(BOOL)animated {
  if (animated)
    [UIView animateWithDuration:0.2 animations:^{
      [self appear];
    }];
  else [self appear];
}

- (void)appear {
  self.backgroundColor = [UIColor whiteColor];
  self.imageView.alpha = 1.0;
  self.activityIndicatorView.alpha = 0.0;
}

@end
