//
//  FVPDFReaderViewController.h
//  FVPDFReaderView
//
//  Created by Ahmed Al Hafoudh on 23.7.2013.
//  Copyright (c) 2013 freevision s.r.o. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MTPDF/MTPDF.h>

#import "FVPageThumbCellView.h"

@interface FVPDFReaderViewController : UIViewController <UIPageViewControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UIGestureRecognizerDelegate> {
  MTPDF *_pdf;
  BOOL _thumbnailsVisible;
  NSUInteger _currentPageIndex;
}

@property (strong, nonatomic) NSString *pdfIdentifier;
@property (strong, nonatomic) NSData *pdfData;

@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (weak, nonatomic) IBOutlet UICollectionView *thumbCollectionView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *slideConstraint;

- (IBAction)backButtonTapped:(id)sender;

@end
