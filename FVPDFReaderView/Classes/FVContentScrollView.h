//
//  FVContentScrollView.h
//  FVPDFReaderView
//
//  Created by Ahmed Al Hafoudh on 24.7.2013.
//  Copyright (c) 2013 freevision s.r.o. All rights reserved.
//

#import <UIKit/UIKit.h>

NSString * const FVToggleThumbnailsRequest;

@interface FVContentScrollView : UIScrollView <UIScrollViewDelegate, UIGestureRecognizerDelegate> {
	CGFloat zoomAmount;
  UIDeviceOrientation currentOrientation;
}

@property (strong, nonatomic) UIImageView *imageView;

- (void)setImage:(UIImage *)image;

@end
