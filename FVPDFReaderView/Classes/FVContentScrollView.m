//
//  FVContentScrollView.m
//  FVPDFReaderView
//
//  Created by Ahmed Al Hafoudh on 24.7.2013.
//  Copyright (c) 2013 freevision s.r.o. All rights reserved.
//

#import "FVContentScrollView.h"

@implementation FVContentScrollView

#pragma mark Constants

#define ZOOM_LEVELS 2
#define CONTENT_INSET 0.0f

NSString * const FVToggleThumbnailsRequest = @"FVToggleThumbnailsRequest";

#pragma mark ReaderContentView functions

static inline CGFloat ZoomScaleThatFits(CGSize target, CGSize source) {
	CGFloat w_scale = (target.width / source.width);
	CGFloat h_scale = (target.height / source.height);
  CGFloat l_scale = (target.height / source.width);
  
  CGFloat scale;
  if (UIInterfaceOrientationIsLandscape([[UIDevice currentDevice] orientation]))
    scale = l_scale;
  else
    scale = ((w_scale < h_scale) ? w_scale : h_scale);
  
	return scale;
}

#pragma mark ReaderContentView instance methods

- (void)updateMinimumMaximumZoom {
	CGRect targetRect = CGRectInset([UIScreen mainScreen].bounds, CONTENT_INSET, CONTENT_INSET);
  
  CGFloat zoomScale = ZoomScaleThatFits(targetRect.size, self.imageView.bounds.size);
  
	self.minimumZoomScale = zoomScale; // Set the minimum and maximum zoom scales
  
	self.maximumZoomScale = (zoomScale * ZOOM_LEVELS); // Max number of zoom levels
  
	zoomAmount = ((self.maximumZoomScale - self.minimumZoomScale) / ZOOM_LEVELS);
}

- (void)awakeFromNib {
  self.scrollsToTop = NO;
  self.delaysContentTouches = NO;
  self.showsVerticalScrollIndicator = NO;
  self.showsHorizontalScrollIndicator = NO;
  self.contentMode = UIViewContentModeRedraw;
  self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
  self.backgroundColor = [UIColor clearColor];
  self.userInteractionEnabled = YES;
  self.autoresizesSubviews = NO;
  self.bouncesZoom = NO;
  self.delegate = self;
  
  [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
  
  [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(deviceOrientationDidChange:) name: UIDeviceOrientationDidChangeNotification object: nil];
  
	UITapGestureRecognizer *oneTouchDoubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
	oneTouchDoubleTapRecognizer.numberOfTouchesRequired = 1; oneTouchDoubleTapRecognizer.numberOfTapsRequired = 2; oneTouchDoubleTapRecognizer.delegate = self;
  
  UITapGestureRecognizer *twoTouchDoubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
	twoTouchDoubleTapRecognizer.numberOfTouchesRequired = 2; twoTouchDoubleTapRecognizer.numberOfTapsRequired = 2; twoTouchDoubleTapRecognizer.delegate = self;
  
  [self addGestureRecognizer:oneTouchDoubleTapRecognizer];
  [self addGestureRecognizer:twoTouchDoubleTapRecognizer];

  UITapGestureRecognizer *toggleThumbnailsRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleThumbnails:)];
	toggleThumbnailsRecognizer.numberOfTouchesRequired = 1; toggleThumbnailsRecognizer.numberOfTapsRequired = 1; toggleThumbnailsRecognizer.delegate = self;
  [toggleThumbnailsRecognizer requireGestureRecognizerToFail:oneTouchDoubleTapRecognizer];
  
  [self addGestureRecognizer:toggleThumbnailsRecognizer];
}

- (void)toggleThumbnails:(id)sender {
  [[NSNotificationCenter defaultCenter] postNotificationName:FVToggleThumbnailsRequest object:nil];
}

- (void)handleDoubleTap:(UITapGestureRecognizer *)recognizer {
	if (recognizer.state == UIGestureRecognizerStateRecognized) {
    if (recognizer.numberOfTouchesRequired == 1)
      [self zoomIncrementWithCenter:[recognizer locationInView:self.imageView]];
    else if (recognizer.numberOfTouchesRequired == 2)
      [self zoomDecrement];
  }
}

- (void)setImage:(UIImage *)image {
  if (self.imageView)
    [self.imageView removeFromSuperview];
  
  self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, image.size.width, image.size.height)];
  
  self.imageView.image = image;
  self.imageView.autoresizesSubviews = NO;
  self.imageView.userInteractionEnabled = NO;
  self.imageView.contentMode = UIViewContentModeRedraw;
  self.imageView.autoresizingMask = UIViewAutoresizingNone;
  self.imageView.backgroundColor = [UIColor whiteColor];
  
  self.contentSize = self.imageView.bounds.size; // Content size same as view size
  self.contentOffset = CGPointMake((0.0f - CONTENT_INSET), (0.0f - CONTENT_INSET)); // Offset
  self.contentInset = UIEdgeInsetsMake(CONTENT_INSET, CONTENT_INSET, CONTENT_INSET, CONTENT_INSET);
  
  [self updateMinimumMaximumZoom]; // Update the minimum and maximum zoom scales
  
  self.zoomScale = self.minimumZoomScale; // Set zoom to fit page content
  
  [self setContentOffset:CGPointZero animated:NO];
  
  [self addSubview:self.imageView];

}

- (void)updateZoomScale {
  CGFloat oldMinimumZoomScale = self.minimumZoomScale;
  
  [self updateMinimumMaximumZoom]; // Update zoom scale limits
  
  if (self.zoomScale == oldMinimumZoomScale) // Old minimum
    self.zoomScale = self.minimumZoomScale;
  else // Check against minimum zoom scale
    if (self.zoomScale < self.minimumZoomScale)
      self.zoomScale = self.minimumZoomScale;
    else // Check against maximum zoom scale
      if (self.zoomScale > self.maximumZoomScale)
        self.zoomScale = self.maximumZoomScale;
}

- (void)deviceOrientationDidChange:(NSNotification *)notification {
  //Obtaining the current device orientation
  UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
  
  //Ignoring specific orientations
  if (orientation == UIDeviceOrientationFaceUp || orientation == UIDeviceOrientationFaceDown || orientation == UIDeviceOrientationUnknown || currentOrientation == orientation) {
    return;
  }
  [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(relayoutLayers) object:nil];
  //Responding only to changes in landscape or portrait
  currentOrientation = orientation;
  //
  [self performSelector:@selector(orientationChangedMethod) withObject:nil afterDelay:0];
}

- (void)orientationChangedMethod {
  [self updateZoomScale];
}

- (void)layoutSubviews {
	[super layoutSubviews];
  
	CGSize boundsSize = self.bounds.size;
	CGRect viewFrame = self.imageView.frame;
  
	if (viewFrame.size.width < boundsSize.width)
		viewFrame.origin.x = (((boundsSize.width - viewFrame.size.width) / 2.0f) + self.contentOffset.x);
	else
		viewFrame.origin.x = 0.0f;
  
	if (viewFrame.size.height < boundsSize.height)
		viewFrame.origin.y = (((boundsSize.height - viewFrame.size.height) / 2.0f) + self.contentOffset.y);
	else
		viewFrame.origin.y = 0.0f;
  
	self.imageView.frame = viewFrame;
}

- (void)zoomIncrementWithCenter:(CGPoint)center {
	CGFloat zoomScale = self.zoomScale;
  
	if (zoomScale < self.maximumZoomScale)
	{
		zoomScale += zoomAmount; // += value
    
		if (zoomScale > self.maximumZoomScale)
			zoomScale = self.maximumZoomScale;
    
    CGRect zoomRect = [self zoomRectToPoint:center withScale:zoomScale];
    [self zoomToRect:zoomRect animated:YES];
	} else
    [self zoomReset];
}

- (void)zoomDecrement {
	CGFloat zoomScale = self.zoomScale;
  
	if (zoomScale > self.minimumZoomScale)
	{
		zoomScale -= zoomAmount; // -= value
    
		if (zoomScale < self.minimumZoomScale)
			zoomScale = self.minimumZoomScale;
    
		[self setZoomScale:zoomScale animated:YES];
	}
}

- (void)zoomReset {
	if (self.zoomScale > self.minimumZoomScale)
    [self setZoomScale:self.minimumZoomScale animated:YES];
}

- (CGRect)zoomRectToPoint:(CGPoint)zoomPoint withScale:(CGFloat)scale {
  
  //Normalize current content size back to content scale of 1.0f
  CGSize contentSize;
  contentSize.width = (self.contentSize.width / self.zoomScale);
  contentSize.height = (self.contentSize.height / self.zoomScale);
  
//  //translate the zoom point to relative to the content rect
//  zoomPoint.x = (zoomPoint.x / self.bounds.size.width) * contentSize.width;
//  zoomPoint.y = (zoomPoint.y / self.bounds.size.height) * contentSize.height;
  
  //derive the size of the region to zoom to
  CGSize zoomSize;
  zoomSize.width = self.bounds.size.width / scale;
  zoomSize.height = self.bounds.size.height / scale;
  
  //offset the zoom rect so the actual zoom point is in the middle of the rectangle
  CGRect zoomRect;
  zoomRect.origin.x = zoomPoint.x - zoomSize.width / 2.0f;
  zoomRect.origin.y = zoomPoint.y - zoomSize.height / 2.0f;
  zoomRect.size.width = zoomSize.width;
  zoomRect.size.height = zoomSize.height;
    
  return zoomRect;
}

#pragma mark UIScrollViewDelegate methods

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return self.imageView;
}

@end
