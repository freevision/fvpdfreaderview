//
//  FVPDFReaderViewController.m
//  FVPDFReaderView
//
//  Created by Ahmed Al Hafoudh on 23.7.2013.
//  Copyright (c) 2013 freevision s.r.o. All rights reserved.
//

#import <TMCache/TMCache.h>

#import "FVPDFReaderViewController.h"

#import "FVPDFDataSource.h"

#import "FVPDFPageViewController.h"

#import "MTPDFPage+FVPDFPageThumbnails.h"

#import "FVContentScrollView.h"

@interface FVPDFReaderViewController ()
@property (readonly, strong, nonatomic) FVPDFDataSource *modelController;
@end

@implementation FVPDFReaderViewController

@synthesize modelController = _modelController;

- (void)viewDidLoad {
  [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
  // Configure the page view controller and add it as a child view controller.
  self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
  self.pageViewController.delegate = self;

  _pdf = [MTPDF PDFWithData:self.pdfData];
  
  _modelController = [[FVPDFDataSource alloc] initWithPDF:_pdf];
  
  NSArray *viewControllers = @[
                               [self.modelController viewControllerAtIndex:0 storyboard:self.storyboard]
                               ];
  
  [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:NULL];
  
  _currentPageIndex = 0;

  self.pageViewController.dataSource = self.modelController;

  [self addChildViewController:self.pageViewController];
  [self.view insertSubview:self.pageViewController.view belowSubview:self.thumbCollectionView];

  
  CGRect pageViewRect = self.view.bounds;
  self.pageViewController.view.frame = pageViewRect;

  [self.pageViewController didMoveToParentViewController:self];

  // Add the page view controller's gesture recognizers to the book view controller's view so that the gestures are started more easily.
  self.view.gestureRecognizers = self.pageViewController.gestureRecognizers;
    
  [[NSNotificationCenter defaultCenter] addObserverForName:FVToggleThumbnailsRequest object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
    [self toggleThumbnails];
  }];
    
  _thumbnailsVisible = NO;
  
  self.navigationBar.alpha = 0.0;
  self.navigationBar.topItem.title = self.title;
  
  self.slideConstraint.constant = 200.0;
  [self.thumbCollectionView layoutIfNeeded];
}

- (FVPDFDataSource *)modelController {
  if (!_modelController)
    _modelController = [[FVPDFDataSource alloc] init];
  
  return _modelController;
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)toggleThumbnails {
  (_thumbnailsVisible ? [self hideThumbnails] : [self showThumbnails]);
  _thumbnailsVisible = !_thumbnailsVisible;
}

- (void)showThumbnails {
  CGFloat height = 0.0;
  
  [UIView animateWithDuration:0.5 animations:^{
    self.navigationBar.alpha = 0.8;
    self.slideConstraint.constant = height;
    [self.thumbCollectionView layoutIfNeeded];
  }];
}

- (void)hideThumbnails {
  CGFloat height = self.thumbCollectionView.frame.size.height;
  
  [UIView animateWithDuration:0.5 animations:^{
    self.navigationBar.alpha = 0.0;
    self.slideConstraint.constant = height;
    [self.thumbCollectionView layoutIfNeeded];
  }];
}

#pragma mark - UICollectionView delegate methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
  return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return [_pdf.pages count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
 
  FVPageThumbCellView *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FVPageThumbCellView" forIndexPath:indexPath];
  [cell reset];
  
  MTPDFPage *page = [_pdf.pages objectAtIndex:indexPath.row];
  
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    NSString *cacheKey = [self cacheKeyForPage:indexPath.row inPDFNamed:self.pdfIdentifier];
    UIImage *thumbImage = [[TMCache sharedCache] objectForKey:cacheKey];
    BOOL isNew = NO;
    
    if (thumbImage == nil) {
      thumbImage = [page thumbnailImageWithSize:CGSizeMake(128.0 * 2.0, 200.0 * 2.0)];
      [[TMCache sharedCache] setObject:thumbImage forKey:cacheKey];
      isNew = YES;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
      if (cell) {
        cell.imageView.image = thumbImage;
        [cell appearAnimated:isNew];
      }
    });
  });
  
  return cell;
 
}

- (NSString *)cacheKeyForPage:(NSUInteger)pageNumber inPDFNamed:(NSString *)pdfName {
  return [NSString stringWithFormat:@"thumb:%@:%d", pdfName, pageNumber];
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {
  
  _currentPageIndex = [self.modelController indexOfViewController:[pageViewController.viewControllers lastObject]];
  
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  if (indexPath.row != _currentPageIndex)
    [self.pageViewController setViewControllers:@[[self.modelController viewControllerAtIndex:indexPath.row storyboard:self.storyboard]]
                                      direction:(indexPath.row > _currentPageIndex ? UIPageViewControllerNavigationDirectionForward : UIPageViewControllerNavigationDirectionReverse)
                                       animated:YES
  
                                     completion:NULL];
  
  _currentPageIndex = indexPath.row;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
  CGPoint loc = [touch locationInView:self.thumbCollectionView];
  return (loc.x > 0.0 && loc.y > 0.0) ? NO : YES;
}

- (IBAction)backButtonTapped:(id)sender {
  [self dismissViewControllerAnimated:YES completion:^{ }];
}

@end
