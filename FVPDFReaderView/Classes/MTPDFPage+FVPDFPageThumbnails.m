//
//  FVPDFPageThumbnails.m
//  FVPDFReaderView
//
//  Created by Ahmed Al Hafoudh on 24.7.2013.
//  Copyright (c) 2013 freevision s.r.o. All rights reserved.
//

#import "MTPDFPage+FVPDFPageThumbnails.h"

@implementation MTPDFPage (FVPDFPageThumbnails)

- (UIImage *)thumbnailImageWithSize:(CGSize)thumbSize {

  UIGraphicsBeginImageContext(thumbSize);
  
  CGContextRef ctx = UIGraphicsGetCurrentContext();
  
  CGContextSaveGState(ctx);
  
  CGContextSetRGBFillColor(ctx, 1.0, 1.0, 1.0, 1.0);
  
  CGPDFPageRef pageRef = self.reference;
  
  const CGRect mediaBox = CGPDFPageGetBoxRect(pageRef, kCGPDFMediaBox);
  const CGRect renderRect = [self scaleAndAlign:mediaBox destinationRect:(CGRect){ .size = thumbSize }];
  
  CGContextTranslateCTM(ctx, 0.0, thumbSize.height);
  CGContextScaleCTM(ctx, 1.0, -1.0);
  
  CGContextTranslateCTM(ctx, - (mediaBox.origin.x - renderRect.origin.x), - (mediaBox.origin.y - renderRect.origin.y));
  CGContextScaleCTM(ctx, renderRect.size.width / mediaBox.size.width, renderRect.size.height / mediaBox.size.height);
  
  CGContextDrawPDFPage(ctx, pageRef);
  
  UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
  
  UIGraphicsEndImageContext();
  
  return image;

}


- (CGRect)scaleAndAlign:(CGRect)imageRect destinationRect:(CGRect)destinationRect {
  CGRect scaledRect = {};
  
  CGSize scaledImageSize = imageRect.size;
  
  float scaleFactor = 1.0;
  
  if(destinationRect.size.width / imageRect.size.width < destinationRect.size.height / imageRect.size.height) {
    scaleFactor = destinationRect.size.width / imageRect.size.width;
  } else {
    scaleFactor = destinationRect.size.height / imageRect.size.height;
  }
  
  scaledImageSize.width *= scaleFactor;
  scaledImageSize.height *= scaleFactor;
  
  scaledRect.size = scaledImageSize;
  
  CGAffineTransform transform = CGAffineTransformMakeScale(1, -1);
  transform = CGAffineTransformTranslate(transform, 0, - destinationRect.size.height);
  
  scaledRect = CGRectApplyAffineTransform(scaledRect, transform);
  
  return scaledRect;
}

@end
