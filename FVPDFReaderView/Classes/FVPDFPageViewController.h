//
//  FVPDFPageViewController.h
//  FVPDFReaderView
//
//  Created by Ahmed Al Hafoudh on 23.7.2013.
//  Copyright (c) 2013 freevision s.r.o. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MTPDF/MTPDF.h>

#import "FVContentScrollView.h"

@interface FVPDFPageViewController : UIViewController <UIScrollViewDelegate>

@property (strong, nonatomic) MTPDFPage *pdfPage;
@property (strong, nonatomic) UIImage *image;

@property (weak, nonatomic) IBOutlet FVContentScrollView *contentView;

- (void)loadPage;

@end
