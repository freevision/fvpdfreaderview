//
//  FVPDFDataSource.h
//  FVPDFReaderView
//
//  Created by Ahmed Al Hafoudh on 23.7.2013.
//  Copyright (c) 2013 freevision s.r.o. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MTPDF/MTPDF.h>

@class FVPDFPageViewController;

@interface FVPDFDataSource : NSObject <UIPageViewControllerDataSource> {
  UIStoryboard *_storyboard;
  
  MTPDF *_pdf;
  NSMutableDictionary *_viewControllers;
  NSMutableArray *_unusedViewControllers;
}

- (id)initWithPDF:(MTPDF *)pdf;

- (FVPDFPageViewController *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard;
- (NSUInteger)indexOfViewController:(FVPDFPageViewController *)viewController;

@end
