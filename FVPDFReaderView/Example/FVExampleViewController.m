//
//  FVExampleViewController.m
//  FVPDFReaderView
//
//  Created by Ahmed Al Hafoudh on 25.7.2013.
//  Copyright (c) 2013 freevision s.r.o. All rights reserved.
//

#import "FVExampleViewController.h"

#import "FVPDFReaderViewController.h"

@interface FVExampleViewController ()

@end

@implementation FVExampleViewController

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  if ([segue.identifier isEqualToString:@"FVExampleOpenPDF"]) {
    FVPDFReaderViewController *viewController = (FVPDFReaderViewController *)segue.destinationViewController;
    
    NSString *pdfFilePath = [[NSBundle mainBundle] pathForResource:@"sample" ofType:@"pdf"];
    NSData *pdfData = [NSData dataWithContentsOfFile:pdfFilePath];
    
    viewController.title = @"Some title";
    viewController.pdfIdentifier = @"sample";
    viewController.pdfData = pdfData;
  }
}

@end
